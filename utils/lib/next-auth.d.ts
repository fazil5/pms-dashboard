import NextAuth from "next-auth";
import { JWT } from "next-auth/jwt";

declare module "next-auth" {
    interface Session {
        user: {
            token: any;
            name: any;
            username: string,
            email: string
        },
        token: string
    }

}


declare module "next-auth/jwt" {
    interface JWT {
        user: {
            username: string,
            email: string
        },
        token: string
    }

}