/** @type {import('next').NextConfig} */
const nextConfig = {
    async rewrites() {
        return [
            {
                source: '/backend/:path*',
                destination: `${process.env.BACKEND_URL}/:path*` // Proxy to Backend
            },
            { source: '/uploads/:path', destination: `${process.env.BACKEND_URL}/uploads/:path*` },
            { source: '/staticfiles/:path', destination: `${process.env.BACKEND_URL}/staticfiles/:path*` },
        ]
    }
}

module.exports = nextConfig
