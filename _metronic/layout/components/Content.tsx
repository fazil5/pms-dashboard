"use client"

import { FC, useEffect } from 'react'
import clsx from 'clsx'
import { useLayout } from '@/_metronic/layout/core'
import { DrawerComponent } from '../../assets/ts/components'
import { WithChildren } from '../../helpers'

const Content: FC<WithChildren> = ({ children }) => {
  const { classes } = useLayout()

  useEffect(() => {
    DrawerComponent.hideAll()
  }, [])

  return (
    <div id='kt_content_container' className={clsx(classes.contentContainer.join(' '))}>
      {children}
    </div>
  )
}

export { Content }
