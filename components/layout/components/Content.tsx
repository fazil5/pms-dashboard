import { FC, useEffect } from 'react'
import { usePathname } from 'next/navigation'
import clsx from 'clsx'
import { useLayout } from '@/_metronic/layout/core'
import { DrawerComponent } from '@/_metronic/assets/ts/components'
import { WithChildren } from '@/_metronic/helpers'

const Content: FC<WithChildren> = ({ children }) => {
  const { classes } = useLayout()
  const pathname = usePathname()
  useEffect(() => {
    DrawerComponent.hideAll()
  }, [pathname])

  return (
    <div id='kt_content_container' className={clsx(classes.contentContainer.join(' '))}>
      {children}
    </div>
  )
}

export { Content }
