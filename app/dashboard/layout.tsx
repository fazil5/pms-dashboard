import { AsideDefault } from "@/_metronic/layout/components/aside/AsideDefault"
import { HeaderWrapper } from "@/_metronic/layout/components/header/HeaderWrapper"
import { Content } from "@/_metronic/layout/components/Content"
import { Footer } from "@/_metronic/layout/components/Footer"
import { PageDataProvider } from "@/_metronic/layout/core"
import { ActivityDrawer } from "@/_metronic/partials/layout/activity-drawer/ActivityDrawer"
import { RightToolbar } from "@/_metronic/partials/layout/RightToolbar"
import { DrawerMessenger } from "@/_metronic/partials/layout/drawer-messenger/DrawerMessenger"
import { ScrollTop } from "@/_metronic/layout/components/ScrollTop"
import { getServerSession } from "next-auth"
import { authOptions } from "../api/auth/[...nextauth]/route"

export default async function LoginLayout({ children }: { children: React.ReactNode }) {
  const session = await getServerSession(authOptions)
  return (
    <PageDataProvider>
      <div className='d-flex flex-column flex-root'>
        <div className='page d-flex flex-row flex-column-fluid'>
          <AsideDefault />
          <div className='wrapper d-flex flex-column flex-row-fluid' id='kt_wrapper'>
            <HeaderWrapper />
            <div id='kt_content' className='content d-flex flex-column flex-column-fluid'>
              <Content>
                {children}
              </Content>
            </div>
            <Footer />
          </div>
        </div>
      </div>
      <ActivityDrawer />
      <RightToolbar />

      {/* <InviteUsers /> */}
      {/* <UpgradePlan /> */}
      <ScrollTop />
    </PageDataProvider>
  )
}