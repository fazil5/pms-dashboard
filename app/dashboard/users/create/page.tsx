'use client'

import React, { useState } from 'react'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { IProfileDetails, userRegistraion } from '@/components/modules/accounts/components/settings/SettingsModel'
import axios from 'axios'
import { useSession } from "next-auth/react"
import apiRoutes from '@/utils/api/routes'


const profileDetailsSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    email: Yup.string().required('Email is required'),
    password: Yup.string().required('Password is required'),
    password_confirm: Yup.string().required('Confirm password is required'),
    role_id: Yup.string().required('Role is required'),
})
function Create() {
    const { data: session } = useSession()
    const [data, setData] = useState<any>({})
    const updateData = (fieldsToUpdate: Partial<IProfileDetails>): void => {
        const updatedData = Object.assign(data, fieldsToUpdate)
        setData(updatedData)
    }

    const [loading, setLoading] = useState(false)
    const formik = useFormik<userRegistraion>({
        initialValues: {
            name: "",
            email: "",
            password: "abcd@123",
            password_confirm: "abcd@123",
            role_id: "2"
        },
        validationSchema: profileDetailsSchema,
        onSubmit: async (values) => {
            try {
                setLoading(true)
                const response = await axios.post(apiRoutes.createUser, values, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${session?.token}`
                    }
                })
                const { data } = response?.data?.data
                console.log(data?.data)
                setLoading(false)
                // setData(updatedData)
            } catch (error) {
                setLoading(false)
            }
        },
    })



    return (
        <div className='card mb-5 mb-xl-10'>
            <div
                className='card-header border-0 cursor-pointer'
                role='button'
                data-bs-toggle='collapse'
                data-bs-target='#kt_account_profile_details'
                aria-expanded='true'
                aria-controls='kt_account_profile_details'
            >
                <div className='card-title m-0'>
                    <h3 className='fw-bolder m-0'>Profile Details</h3>
                </div>
            </div>

            <div id='kt_account_profile_details' className='collapse show'>
                <form onSubmit={formik.handleSubmit} noValidate className='form'>
                    <div className='card-body border-top p-9'>


                        <div className='row mb-6'>
                            <label className='col-lg-4 col-form-label required fw-bold fs-6'>Full Name</label>

                            <div className='col-lg-8'>
                                <div className='row'>
                                    <div className='col-lg-6 fv-row'>
                                        <input
                                            type='text'
                                            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                            placeholder='Name'
                                            {...formik.getFieldProps('name')}
                                        />
                                        {formik.touched.name && formik.errors.name && (
                                            <div className='fv-plugins-message-container'>
                                                <div className='fv-help-block'>{formik.errors.name}</div>
                                            </div>
                                        )}
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div className='row mb-6'>
                            <label className='col-lg-4 col-form-label required fw-bold fs-6'>Email</label>
                            <div className='col-lg-8 fv-row'>
                                <input
                                    type='text'
                                    className='form-control form-control-lg form-control-solid'
                                    placeholder='Email'
                                    {...formik.getFieldProps('email')}
                                />
                                {formik.touched.email && formik.errors.email && (
                                    <div className='fv-plugins-message-container'>
                                        <div className='fv-help-block'>{formik.errors.email}</div>
                                    </div>
                                )}
                            </div>
                        </div>

                        <div className='row mb-6'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className='required'>Password</span>
                            </label>

                            <div className='col-lg-8 fv-row'>
                                <input
                                    type='password'
                                    className='form-control form-control-lg form-control-solid'
                                    placeholder='password'
                                    {...formik.getFieldProps('password')}
                                />
                                {formik.touched.password && formik.errors.password && (
                                    <div className='fv-plugins-message-container'>
                                        <div className='fv-help-block'>{formik.errors.password}</div>
                                    </div>
                                )}
                            </div>
                        </div>

                        <div className='row mb-6'>
                            <label className='col-lg-4 col-form-label fw-bold fs-6'>
                                <span className='required'>Confirm Password</span>
                            </label>

                            <div className='col-lg-8 fv-row'>
                                <input
                                    type='password'
                                    className='form-control form-control-lg form-control-solid'
                                    placeholder='Confirm Password'
                                    {...formik.getFieldProps('password_confirm')}
                                />
                                {formik.touched.password_confirm && formik.errors.password_confirm && (
                                    <div className='fv-plugins-message-container'>
                                        <div className='fv-help-block'>{formik.errors.password_confirm}</div>
                                    </div>
                                )}
                            </div>
                        </div>

                        <div className='row mb-6'>
                            <label className='col-lg-4 col-form-label required fw-bold fs-6'>Roles</label>

                            <div className='col-lg-8 fv-row'>
                                <select
                                    className='form-select form-select-solid form-select-lg'
                                    {...formik.getFieldProps('role_id')}
                                >
                                    <option value=''>Select role</option>
                                    <option value='2'>role 2 </option>
                                </select>
                                {formik.touched.role_id && formik.errors.role_id && (
                                    <div className='fv-plugins-message-container'>
                                        <div className='fv-help-block'>{formik.errors.role_id}</div>
                                    </div>
                                )}
                            </div>
                        </div>


                    </div>

                    <div className='card-footer d-flex justify-content-end py-6 px-9'>
                        <button type='submit' className='btn btn-primary' disabled={loading}>
                            {!loading && 'Save Changes'}
                            {loading && (
                                <span className='indicator-progress' style={{ display: 'block' }}>
                                    Please wait...{' '}
                                    <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                                </span>
                            )}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Create