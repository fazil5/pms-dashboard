'use client'

import { KTCardBody } from '@/_metronic/helpers'
import { useMemo, useState, useEffect } from 'react'
// import {useQueryResponseData, useQueryResponseLoading} from '@/_metronic/partials/'
import { usersColumns } from '@/components/modules/apps/user-management/users-list/table/columns/_columns'
import { useTable, ColumnInstance, Row } from 'react-table'
import { CustomHeaderColumn } from '@/components/modules/apps/user-management/users-list/table/columns/CustomHeaderColumn'
import { CustomRow } from '@/components/modules/apps/user-management/users-list/table/columns/CustomRow'
import { UsersListLoading } from '@/components/modules/apps/user-management/users-list/components/loading/UsersListLoading'
// import { UsersListLoading } from '../components/loading/UsersListLoading'
import { UsersListPagination } from '@/components/modules/apps/user-management/users-list/components/pagination/UsersListPagination'
import axios from 'axios'
import { useSession } from "next-auth/react"


// const users = [
//     {
//         "id": 1,
//         "name": "Emma Smith",
//         "avatar": "avatars\/300-6.jpg",
//         "email": "smith@kpmg.com",
//         "position": "Art Director",
//         "role": "Administrator",
//         "last_login": "Yesterday",
//         "two_steps": false,
//         "joined_day": "10 Nov 2022, 9:23 pm",
//         "online": false
//     },
//     {
//         "id": 2,
//         "name": "Melody Macy",
//         "initials": {
//             "label": "M",
//             "state": "danger"
//         },
//         "email": "melody@altbox.com",
//         "position": "Marketing Analytic",
//         "role": "Analyst",
//         "last_login": "20 mins ago",
//         "two_steps": true,
//         "joined_day": "10 Nov 2022, 8:43 pm",
//         "online": false
//     },
//     {
//         "id": 3,
//         "name": "Max Smith",
//         "avatar": "avatars\/300-1.jpg",
//         "email": "max@kt.com",
//         "position": "Software Enginer",
//         "role": "Developer",
//         "last_login": "3 days ago",
//         "two_steps": false,
//         "joined_day": "22 Sep 2022, 8:43 pm",
//         "online": false
//     },
//     {
//         "id": 4,
//         "name": "Sean Bean",
//         "avatar": "avatars\/300-5.jpg",
//         "email": "sean@dellito.com",
//         "position": "Web Developer",
//         "role": "Support",
//         "last_login": "5 hours ago",
//         "two_steps": true,
//         "joined_day": "21 Feb 2022, 6:43 am",
//         "online": false
//     },
//     {
//         "id": 5,
//         "name": "Brian Cox",
//         "avatar": "avatars\/300-25.jpg",
//         "email": "brian@exchange.com",
//         "position": "UI\/UX Designer",
//         "role": "Developer",
//         "last_login": "2 days ago",
//         "two_steps": true,
//         "joined_day": "10 Mar 2022, 9:23 pm",
//         "online": false
//     },
//     {
//         "id": 6,
//         "name": "Mikaela Collins",
//         "initials": {
//             "label": "M",
//             "state": "warning"
//         },
//         "email": "mik@pex.com",
//         "position": "Head Of Marketing",
//         "role": "Administrator",
//         "last_login": "5 days ago",
//         "two_steps": false,
//         "joined_day": "20 Dec 2022, 10:10 pm",
//         "online": false
//     },
//     {
//         "id": 7,
//         "name": "Francis Mitcham",
//         "avatar": "avatars\/300-9.jpg",
//         "email": "f.mit@kpmg.com",
//         "position": "Software Arcitect",
//         "role": "Trial",
//         "last_login": "3 weeks ago",
//         "two_steps": false,
//         "joined_day": "10 Nov 2022, 6:43 am",
//         "online": false
//     },
//     {
//         "id": 8,
//         "name": "Olivia Wild",
//         "initials": {
//             "label": "O",
//             "state": "danger"
//         },
//         "email": "olivia@corpmail.com",
//         "position": "System Admin",
//         "role": "Administrator",
//         "last_login": "Yesterday",
//         "two_steps": false,
//         "joined_day": "19 Aug 2022, 11:05 am",
//         "online": false
//     },
//     {
//         "id": 9,
//         "name": "Neil Owen",
//         "initials": {
//             "label": "N",
//             "state": "primary"
//         },
//         "email": "owen.neil@gmail.com",
//         "position": "Account Manager",
//         "role": "Analyst",
//         "last_login": "20 mins ago",
//         "two_steps": true,
//         "joined_day": "25 Oct 2022, 10:30 am",
//         "online": false
//     },
//     {
//         "id": 10,
//         "name": "Dan Wilson",
//         "avatar": "avatars\/300-23.jpg",
//         "email": "dam@consilting.com",
//         "position": "Web Desinger",
//         "role": "Developer",
//         "last_login": "3 days ago",
//         "two_steps": false,
//         "joined_day": "19 Aug 2022, 10:10 pm",
//         "online": false
//     }
// ]

function Users() {
    // const users = useQueryResponseData()
    const { data: session } = useSession()

    const [users, setusers] = useState([])
    const [isLoading, setisLoading] = useState(true)

    const data = useMemo(() => users, [users])
    const columns = useMemo(() => usersColumns, [])

    const { getTableProps, getTableBodyProps, headers, rows, prepareRow } = useTable({
        columns,
        data,
    })

    useEffect(() => {
        session?.token && getUsers()
    }, [session])


    const getUsers = async () => {
        try {
            const response = await axios.post("/backend/user/table", {}, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${session?.token}`
                }
            })
            const { data } = response?.data?.data
            setusers(data)
            setisLoading(false)
        } catch (error) {
            setisLoading(false)
            console.log(error)
        }
    }


    return (
        <KTCardBody className='py-4'>
            <div className='table-responsive'>
                <table
                    id='kt_table_users'
                    className='table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer'
                    {...getTableProps()}
                >
                    <thead>
                        <tr className='text-start text-muted fw-bolder fs-7 text-uppercase gs-0'>
                            {headers.map((column: ColumnInstance<any>,index) => (
                                <CustomHeaderColumn key={index} column={column} />
                            ))}
                        </tr>
                    </thead>
                    <tbody className='text-gray-600 fw-bold' {...getTableBodyProps()}>
                        {rows.length > 0 ? (
                            rows.map((row: Row<any>, i) => {
                                prepareRow(row)
                                return <CustomRow row={row} key={`row-${i}-${row.id}`} />
                            })
                        ) : (
                            <tr>
                                <td colSpan={7}>
                                    <div className='d-flex text-center w-100 align-content-center justify-content-center'>
                                        No matching records found
                                    </div>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
            <UsersListPagination />
            {isLoading && <UsersListLoading />}
        </KTCardBody>
    )
}

export default Users