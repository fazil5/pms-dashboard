import { Backend_URL } from "@/utils/constants";
import { NextAuthOptions } from "next-auth";
import { JWT } from "next-auth/jwt";
import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials";

async function refreshToken(token: any): Promise<JWT> {
    const res = await fetch(Backend_URL + "/auth/refresh", {
        method: "POST",
        headers: {
            authorization: `Refresh ${token?.backendTokens?.refreshToken}`,
        },
    });
    console.log("refreshed");

    const response = await res.json();

    return {
        ...token,
        backendTokens: response,
    };
}

export const authOptions: NextAuthOptions = {
    providers: [
        CredentialsProvider({
            name: "Credentials",
            credentials: {
                username: {
                    label: "Username",
                    type: "text",
                },
                password: { label: "Password", type: "password" },
            },
            async authorize(credentials: any, req: any) {
                const { email, password } = credentials;
                if (!email || !password) return null;
                try {
                    const res = await fetch(Backend_URL + "/auth/token", {
                        method: "POST",
                        body: JSON.stringify({
                            deviceIdentity: "auth_token",
                            email,
                            password,
                        }),
                        headers: {
                            "Content-Type": "application/json",
                        },
                    });

                    if (res.status == 401) {
                        console.log(res?.statusText, 'res status code');
                        return null;
                    }
                    const result = await res.json();
                    console.log(result, 'result');

                    if (result?.status) {
                        const user = result?.data
                        console.log(user, "user")
                        return user;
                    }
                    return null
                } catch (error) {
                    console.log(error)
                    return null

                }
            },
        }),
    ],
    callbacks: {
        async jwt({ token, user }: any) {
            if (user) return { ...token, ...user };
            return token
            if (new Date().getTime() < token.expiresIn)
                return token;

            // return await refreshToken(token);
        },

        async session({ token, session }: any) {
            session.user = token.user;
            session.token = token.access_token;
            return session;
        },
    },
    pages: {
        signIn: "/auth/login"
    }
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };